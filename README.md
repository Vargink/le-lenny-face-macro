# Le ( ͡° ͜ʖ ͡°) Face Replacement Macro #

This is just a simple [autoit](https://www.autoitscript.com/site/autoit/) script that replaces the word "lenny" with "( ͡° ͜ʖ ͡°)" is it a fork from [here](https://github.com/jvanegmond/hotstrings/) and uses hoststrings to make this a lot easier.
Feel free to compile this yourself or use the provided compiled exe in the [downloads](https://bitbucket.org/Vargink/le-lenny-face-macro/downloads/) area or do it yourself with [autoit](https://www.autoitscript.com/site/autoit/).

### Who do I talk to? ###

* [Vargink](https://keybase.io/theguywho)