; Just a simple ( ͡° ͜ʖ ͡°) face replacer modified by Vargink 
; Get the latest of this from https://bitbucket.org/Vargink/le-lenny-face-macro

; Get the latest hotstring from https://github.com/jvanegmond/hotstrings/
#include <HotString.au3>

HotStringSet("lenny", lelennify)

While 1
    Sleep(10)
WEnd

Func lelennify()
    Send("{BACKSPACE}{BACKSPACE}{BACKSPACE}{BACKSPACE}{BACKSPACE}")
    Send("( ͡° ͜ʖ ͡°)")
EndFunc